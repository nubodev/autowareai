FROM alpine
ADD opt.tar.gz /
RUN cp /opt/AutowareAI/setup.bash /opt/AutowareAI/.env.sh
VOLUME /opt/AutowareAI
CMD ["/bin/sh", "-c", "trap 'exit 147' TERM; tail -f /dev/null & wait ${!}"]
