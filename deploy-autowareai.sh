#!/bin/bash

CI_REGISTRY=${CI_REGISTRY:-registry.gitlab.com}
CI_REGISTRY_IMAGE=${CI_REGISTRY_IMAGE:-registry.gitlab.com/nubodev/autowareai}
IMAGE_NAME=$CI_REGISTRY_IMAGE:latest

# build the Docker image (this will use the Dockerfile in the root of the repo)
docker build -t $IMAGE_NAME .
# authenticate with the Gitlab registry
docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" $CI_REGISTRY
# push the new Docker image to the Gitlab registry
docker push $IMAGE_NAME